
import { GetStaticProps } from 'next'
import { FC } from 'react'

import PostsService from 'services/PostService'
import { NewType } from 'pages/news/[slug]'
import NewsItem from 'components/NewsItem'

type NewsPageType = {
	news: {
		data: NewType
	}[]
}

import styles from './styles.module.scss'

const AllNewsPage: FC<NewsPageType> = ({ news }) => {
	return (
		<div className={styles['all-news-page']}>
			<div className={styles['title']}>
				Новости
			</div>

			<div className={styles['news']}>
				{
					news.map((item, index) =>
						<NewsItem {...item.data} key={index} />

					)
				}
			</div>
		</div>
	)
}

export const getStaticProps: GetStaticProps = async () => {

	PostsService.postsDirectory = '_posts/news'

	const allNews = PostsService.getAllPosts()

	return {
		props: {
			news: allNews
		}
	}
}

export default AllNewsPage