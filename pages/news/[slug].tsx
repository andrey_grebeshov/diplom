import { GetStaticPaths, GetStaticProps } from 'next'
import Image from 'next/image'
import { FC } from 'react'

import PostsService from 'services/PostService'

export type NewType = {
	id: string
	title: string
	date: string
	image: string
}

type NewPageType = {
	data: NewType
	content: string
}

import styles from './styles.module.scss'

const NewsPage: FC<NewPageType> = (props) => {

	const { data, content } = props

	const { title, date, image } = data

	return (
		<div className={styles['news__wrap']}>
			<div className={styles['news']}>
				<div className={styles['title']}>
					{title}
				</div>
				<div className={styles['date']}>
					{date}
				</div>

				<div className={styles['image']}>
					<Image src={image} layout='fill' objectFit='fill' />
				</div>

				<div className={styles['content']} dangerouslySetInnerHTML={{ __html: content }} />
			</div>

		</div>
	)
}

export const getStaticPaths: GetStaticPaths = async () => {

	PostsService.postsDirectory = '_posts/news'

	const paths = PostsService.getPostPaths()

	return {
		paths,
		fallback: false
	}
}

export const getStaticProps: GetStaticProps = async ({ params }) => {

	if (params?.slug) {

		PostsService.postsDirectory = '_posts/news'

		const { data, content } = PostsService.getPostBySlug(params.slug.toString(), true)

		return {
			props: {
				data,
				content,
			},
		}
	} else {
		return {
			notFound: true
		}
	}
}

export default NewsPage