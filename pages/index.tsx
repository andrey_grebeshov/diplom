import { FC, useEffect, useState } from 'react'
import axios from 'axios'
import classNames from 'classnames'
import Image from 'next/image'

import AboutCompany from 'assets/images/AboutCompany/AboutCompany.png'
import AboutCompany2 from 'assets/images/AboutCompany/AboutCompany2.png'
import AboutCompany3 from 'assets/images/AboutCompany/AboutCompany3.png'

import Button from 'components/ui/Button'


import NewsItem from 'components/NewsItem'
import AdvantageBlock from 'components/AdvantageBlock'



export type AboutCompanyProps = {
	id: number
	text: string
	image: string
}

export type AdvantageProps = {
	id: number
	icon: 'icon_rocket' |
	'icon_check' |
	'icon_finance' |
	'icon_quality' |
	'icon_order'
	title: string
	text: string
}

export type NewsItemProps = {
	id: number
	image: string
	title: string
	date: string
}

export type DataProps = {
	about_company: AboutCompanyProps[]
	advantages: AdvantageProps[]
	news: NewsItemProps[]
}

import styles from './styles.module.scss'
import { GetStaticProps } from 'next'
import PostsService from 'services/PostService'
import { NewType } from 'pages/news/[slug]'

type MainPageType = {
	news: {
		data: NewType
	}[]
}


const MainPage: FC<MainPageType> = ({ news }) => {

	const [data, setData] = useState<DataProps>()

	const getAboutCompanyData = async () => {

		try {
			const { data } = await axios.get(`https://mihailpov123.github.io/apidata/db.json`);

			setData(data)

		} catch (error) {

			console.log('error', error);

		}

	}


	useEffect(() => {

		getAboutCompanyData()

	}, [])


	return (
		<div className={styles['main-page']}>

			<div className={styles['aboutCompany__wrap']}>
				<div className={classNames(styles['aboutCompany'], styles['container'])}>
					<div className={styles['title']}>
						О компании
					</div>
					<div className={styles['aboutCompanyBlocks']}>
						<div className={styles['aboutBlocks']}>
							<div className={classNames(styles['about-company-block'])}>
								<div className={classNames(styles['block'], styles['block--text'])}>
									Более 13 лет мы оптимизируем и запускаем новые технологические производственные процессы, поставляем инструмент и оснастку от ведущих мировых производителей для реализации задач вашего производства. Ключевой ресурс компании - ценные кадры. В нашем деле, для того чтобы двигаться вперёд, необходима надёжная, сплочённая команда профессионалов своего дела. Теперь, наши специалисты в 15 городах России.
								</div>
								<div className={classNames(styles['block'], styles['block--image'])}>
									<div className={styles['image']}>
										<Image src={AboutCompany} layout='fill' objectFit='contain' />
									</div>
								</div>
							</div>
							<div className={styles['aboutBlocks']}>
								<div className={classNames(styles['about-company-block'], styles['about-company-block--reverse'])}>
									<div className={classNames(styles['block'], styles['block--text'])}>
										Наши клиенты – это наши партнёры. Вместе мы делаем одно большое дело. Мы стремимся к повышению конкурентоспособности российской продукции и интеграции ее в мировую экономику, снижая затраты на ее изготовление. Сбалансированный подход к инструментообеспечению – первый шаг на пути к эффективному производству.
									</div>
									<div className={classNames(styles['block'], styles['block--image'])}>
										<div className={styles['image']}>
											<Image src={AboutCompany2} layout='fill' objectFit='contain' />
										</div>
									</div>
								</div>
								<div className={styles['aboutBlocks']}>
									<div className={classNames(styles['about-company-block'])}>
										<div className={classNames(styles['block'], styles['block--text'])}>
											С нами ваше производство будет работать эффективнее. Мы глубоко изучили рынок маталлообработки в Росии, переняли опыт у наших зарубежных коллег и в итоге выработали наиболее эффективный подход, учитывающий нюансы отдельно взятого предприятия. Оптимизация даже одного производственного процесса приводит к увеличению эффективности всего производтсва.
										</div>
										<div className={classNames(styles['block'], styles['block--image'])}>
											<div className={styles['image']}>
												<Image src={AboutCompany3} layout='fill' objectFit='contain' />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className={classNames(styles['advantages'], styles['container'])}>
							<div className={styles['title']}>
								Наши преимущества
							</div>
							<div className={styles['advantages__content']}>
								{
									data?.advantages.map(advantage =>
										<AdvantageBlock {...advantage} key={advantage.id} />

									)
								}
							</div>
						</div>

						<div className={classNames(styles['news'], styles['container'])}>
							<div className={styles['title']}>
								Новости
							</div>
							<div className={styles['news__wrap']}>
								{
									news.map(item =>
										<NewsItem {...item.data} key={item.data.id} />
									)
								}
							</div>
							<Button className={styles['btnAllNews']}
								text='Все новости' />
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export const getStaticProps: GetStaticProps = async () => {

	PostsService.postsDirectory = '_posts/news'

	const allNews = PostsService.getAllPosts()

	return {
		props: {
			news: allNews
		}
	}
}

export default MainPage
