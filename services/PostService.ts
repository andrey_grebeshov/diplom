import matter from 'gray-matter'
import { join } from 'path'
import fs from 'fs'

/**
 * PostsService
 * 
 * Класс для работы с постами
*/

class PostsService {

	static postsDirectory = ''

	/**
	 * Метод для получения названий постов
	 * @returns {string[]} массив с названием постов
	*/
	static getPostSlugs() {
		return fs.readdirSync(join(process.cwd(), PostsService.postsDirectory))
	}

	/**
	 * Метод для получения путей постов
	 * @param {string[] | undefined} locales 
	 * @returns {{params: {slug: string;};locale: string;}[]} пути постов
	*/
	static getPostPaths() {

		const slugs = PostsService.getPostSlugs()

		let paths: { params: { slug: string } }[] = []

		slugs.map(slug => {
			paths.push({ params: { slug } })
		})

		return paths
	}

	/**
	* Метод для получения поста по его названию
	* @param {string} slug 
	* @param {boolean} getContent 
	* @returns {Items} пост
	*/
	static getPostBySlug(slug: string, getContent: boolean = false) {
		const realSlug = slug
		const fullPath = join(join(process.cwd(), PostsService.postsDirectory), `${realSlug}/index.md`)

		const fileContents = fs.readFileSync(fullPath, 'utf8')

		const { data, content } = matter(fileContents)

		if (getContent) {
			return { data, content }
		}

		return { data }
	}

	/**
	* Метод для получения всех постов
	* @param {boolean} getContent 
	* @returns {Items[]} массив постов
	*/
	static getAllPosts(getContent: boolean = false) {
		const slugs = PostsService.getPostSlugs()

		const posts = slugs
			.map((slug) => PostsService.getPostBySlug(slug, getContent))
			.sort((post1, post2) => (post1.data.date > post2.data.date ? -1 : 1))

		return posts
	}
}

export default PostsService