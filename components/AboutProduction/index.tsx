import axios from 'axios';
import AboutProductionBlock from 'components/AboutProductionBlock';
import { useEffect, useState } from 'react';
import styles from './styles.module.scss'
import { StaticImageData } from 'next/image';
import AboutCompany from 'assets/images/AboutProduction/AboutProduction.png'
import AboutCompany2 from 'assets/images/AboutProduction/AboutProduction2.png'
import AboutCompany3 from 'assets/images/AboutProduction/AboutProduction3.png'


export type AboutProductionProps = {
	id: number
	title: string
	image: StaticImageData
}

const AboutProduction = () => {

	const [production, setProduction] = useState<AboutProductionProps[]>()

	const getProductionData = async () => {

		try {
			const { data } = await axios.get(`https://mihailpov123.github.io/apidata/production.json`);

			setProduction(data.production)

		} catch (error) {

			console.log('error', error);

		}

	}


	useEffect(() => {

		getProductionData()

	}, [])

	return (
		<div className={styles['about-production']}>
			<div className={styles['title']}>
				О производстве
			</div>

			<div className={styles['blocks']}>

				<AboutProductionBlock
					id={1}
					image={AboutCompany}
					title='Производтсво типовых изделий'
					content='«ГК Инструмент» предлагает широкий выбор типовых изделий.  В нашем каталоге представлены все возможные конфигурации стройматериалов, представляющих неизменно высокий интерес для промышленного, жилого и гражданского строительства. Кроме продаж имеющегося ассортимента товаров, мы также принимаем заказы на изготовление изделий по образцу заказчика.'
				/>
				<AboutProductionBlock
					id={2}
					image={AboutCompany2}
					title='Доставка продукции транспортом'
					content='Для организации доставки продукции создан логистический центр. Для доставки на строительные объекты в пределах Свердловской области мы используем собственный автопарк — это позволяет предоставлять заказчикам недорогие услуги по перевозке в оптимальные сроки. В то же время в нашей компании внедрена эффективная логистическая схема взаимодействия с надежными партнерами по междугородним автомобильным или железнодорожным грузоперевозкам. Это дает отличную возможность поставлять крупные партии на строительные объекты в соседние и даже относительно удаленные регионы по приемлемым ценам.'
				/>
				<AboutProductionBlock
					id={3}
					image={AboutCompany3}
					title='Производство изделий'
					content='ГК Инструмент, кроме продажи товаров, имеющихся на складе в наличии, принимает также заказына изготовление прочих стандартных и нестандартных изделий по индивидуальным чертежам и образцом заказчика. Мы готовы взяться за разработку, изготовление и поставку изделий любой тенлогической сложности в договорные сроки и с гарантией неизменно выского качества нашей продукции. Самое современное оборудование и собственная лаборатрия по контролю над качеством, а главное, высококвалифицированные специалисты и инженеры компании позволяют нам обеспечивать такую возможность'
				/>
			</div>



		</div>
	)
}

export default AboutProduction