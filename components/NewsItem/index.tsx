import { useRouter } from 'next/router'
import Image from 'next/image'
import { FC } from 'react'

import { NewType } from 'pages/news/[slug]'
import Button from 'components/ui/Button'

import styles from './styles.module.scss'

const NewsItem: FC<NewType> = ({ id, image, title, date }) => {

	const router = useRouter()

	return (
		<div className={styles['news-item']}>
			<div className={styles['image']}>
				<Image src={image} layout='fill' alt='image' objectFit='contain' />
			</div>
			<div className={styles['title']}>
				{title}
			</div>
			<div className={styles['date']}>
				{date}
			</div>
			<Button
				onClick={() => router.push(`news/${id}`)}
				className={styles['btnMore']}
				text='Подробнее'
			/>
		</div>
	)
}

export default NewsItem